# translation template for thunar-favs-plugin 
# Copyright (C) 2009-2013 Alexander Nagel
# This file is distributed under the same license as the PACKAGE package.
# Alexander Nagel <alexander@acwn.de>, 2013.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: thunar-favs-plugin 0.0.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2013-05-29 21:45+0200\n"
"PO-Revision-Date: 2013-05-30 10:00+0200\n"
"Last-Translator: Alexander Nagel <alexander@acwn.de>\n"
"Language-Team: de <de@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: src/thunar-favs.c:115
msgid "Could not execute Filemanager-AVS"
msgstr ""

#: src/thunar-favs.c:129
msgid "Scan folder for viruses"
msgstr ""

#: src/thunar-favs.c:129
msgid "Open Filemanager-AVS in this folder"
msgstr ""

#: src/thunar-favs.c:156
msgid "Scan file for viruses"
msgstr ""

#: src/thunar-favs.c:156
msgid "Open Filemanager-AVS to scan this file"
msgstr ""

#: src/thunar-favs-plugin.c:47
#, c-format
msgid "The version doesn't match: %s"
msgstr ""

#: src/thunar-favs-plugin.c:52
msgid "Register Filemanager-AVS plugin"
msgstr ""

#: src/thunar-favs-plugin.c:62
msgid "Shutting down Filemanager-AVS plugin"
msgstr ""
